<?php


namespace MVC\Controllers;


use MVC\Views\View;
use MVC\Routes\WebRouter;

abstract class Controller
{

    protected $view;
    protected array $except = [];

    public function __construct()
    {
        if (!in_array(WebRouter::$currentRouteUrl, $this->except)) {
            $this->before();
        }
        $this->view = new View();
    }

    abstract protected function before();
}