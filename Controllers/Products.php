<?php


namespace MVC\Controllers;

use MVC\Models\Product;
use MVC\Models\Category;

class Products  extends Controller
{
    public function index(array $request = [])
    {
        $data = [];
        if (!empty($request['cat_id'])) {
            $data['products'] = Product::getByField('category_id', (int)$request['cat_id']);
        } else {
            $data['products'] = Product::getAll();
        }
        $data['categories'] = Category::getAll();
        $this->view->render('products', $data);
    }

    protected function before()
    {
        if (empty($_SESSION['user'])) {
            header('Location: /user/login');
            die();
        }
    }
}