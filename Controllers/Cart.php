<?php


namespace MVC\Controllers;

use MVC\Models\Product;
use MVC\Models\Cart as CartModel;
use MVC\Models\CartProduct;


class Cart  extends Controller
{
    public function index(array $request = [])
    {
        if (empty($_SESSION['cart_id'])) {
            header('Location: /');
            die();
        }
        $data = [];
        $data['selectedProducts'] = CartProduct::getAllProducts($_SESSION['cart_id']);
        $this->view->render('cart', $data);
    }

    public function add(array $request = [])
    {
        $data = [];
        if (!empty($request['products'])) {
            if (empty($_SESSION['cart_id'])) {
                $cart = CartModel::create($_SESSION['user']->id);
                if(!empty($cart)){
                    $_SESSION['cart_id'] = $cart->getId();
                }
            }
            CartProduct::add($_SESSION['cart_id'], $request['products']);
        }
        header('Location: /cart');
        die();
    }

    protected function before()
    {
        if (empty($_SESSION['user'])) {
            header('Location: /user/login');
            die();
        }
    }
}