<?php


namespace MVC\Controllers;

use MVC\Models\User as UserModel;

class User extends Controller
{
    protected array $except = ['user/logout'];

    public function login(array $request = [])
    {
        $data = [];
        if (!empty($request['email']) && !empty($request['password'])) {
            $user = UserModel::login($request['email'], $request['password']);
            $data['error'] = 0;
            if (!empty($user)) {
                $_SESSION['user'] = $user;
                header('Location: /');
                die();
            } else {
                $data['error'] = "Login credentials are invalid";
            }
        }
        $this->view->render('login', $data);
    }

    public function register(array $request = [])
    {
        $fields = ["full_name", "email", "password", "password_confirm"];
        $data = [];
        if (!empty($request)) {
            $data['errors'] = [];
            foreach ($fields as $field) {
                if (empty($request[$field])) {
                    $data['errors'][] = "Field " . ucwords(str_replace("_", " ", $field)) . " is required";
                }
            }

            if (!empty($request['email']) && UserModel::checkIfUnique($request['email'], 'users', 'email')) {
                $data['errors'][] = "Email must be unique";
            }

            if ($request['password'] !== $request['password_confirm']) {
                $data['errors'][] = "Password must be confirmed";
            }

            if (empty($data['errors'])) {
                $user = UserModel::save($request['full_name'], $request['email'], $request['password']);
                $_SESSION['user'] = UserModel::getById($user->id);
                header('Location: /');
                die();
            }
        }
        $this->view->render('registration', $data);
    }

    public function logout()
    {
        unset($_SESSION['user']);
        header('Location: /');
        die();
    }


    protected function before()
    {
        if (!empty($_SESSION['user'])) {
            header('Location: /');
            die();
        }
    }
}