<?php


namespace MVC\Routes;


class WebRouter
{
    private array $routes = [];
    public static string $currentRouteUrl = '';

    public function __construct($routes)
    {
        foreach ($routes as $url => $route) {
            $this->add($url, $route['controller'], $route['action']);
        }
    }

    public function add(string $url, string $controller, string $action): void
    {
        if (!$this->check($url)) {
            $this->routes[$url] = ["controller" => $controller, "action" => $action];
        }
    }

    public function check(string $url): bool|array
    {
        return !empty($this->routes[$url]) ? $this->routes[$url] : false;
    }

    public function run(array $request)
    {
        if (!($route = $this->check($request['path']))) {
            header("HTTP/1.0 404 Not Found");
            die();
        }
        self::$currentRouteUrl = $request['path'];
        unset($request['path']);
        $controllerName = "MVC\\Controllers\\" . ucfirst($route['controller']);
        $controller = new $controllerName;
        $controller->{$route['action']}($request);
    }

}