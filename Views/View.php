<?php


namespace MVC\Views;


class View
{
    private $templateFolder = 'templates';
    private $templateExtension = '.php';
    private $header = "header";
    private $footer = "footer";
    private $partialsFolder = "partials";

    /**
     * @return string
     */
    public function getHeader(): string
    {
        return $this->header;
    }

    /**
     * @param string $header
     */
    public function setHeader(string $header): void
    {
        $this->header = $header;
    }

    /**
     * @return string
     */
    public function getFooter(): string
    {
        return $this->footer;
    }

    /**
     * @param string $footer
     */
    public function setFooter(string $footer): void
    {
        $this->footer = $footer;
    }

    /**
     * @return string
     */
    public function getPartialsFolder(): string
    {
        return $this->partialsFolder;
    }

    /**
     * @param string $partialsFolder
     */
    public function setPartialsFolder(string $partialsFolder): void
    {
        $this->partialsFolder = $partialsFolder;
    }

    /**
     * @return string
     */
    public function getTemplateFolder(): string
    {
        return $this->templateFolder;
    }

    /**
     * @param string $templateFolder
     */
    public function setTemplateFolder(string $templateFolder): void
    {
        $this->templateFolder = $templateFolder;
    }

    /**
     * @return string
     */
    public function getTemplateExtension(): string
    {
        return $this->templateExtension;
    }

    /**
     * @param string $templateExtension
     */
    public function setTemplateExtension(string $templateExtension): void
    {
        $this->templateExtension = $templateExtension;
    }

    public function render($view, $data = [])
    {
        if (!empty($data)) {
            extract($data);
        }
        require_once(ROOT_PATH . DIRECTORY_SEPARATOR . $this->templateFolder . DIRECTORY_SEPARATOR . $this->partialsFolder .
            DIRECTORY_SEPARATOR . $this->header . $this->templateExtension);
        require_once(ROOT_PATH . DIRECTORY_SEPARATOR . $this->templateFolder . DIRECTORY_SEPARATOR . $view .
            $this->templateExtension);
        require_once(ROOT_PATH . DIRECTORY_SEPARATOR . $this->templateFolder . DIRECTORY_SEPARATOR . $this->partialsFolder .
            DIRECTORY_SEPARATOR . $this->footer . $this->templateExtension);
    }
}