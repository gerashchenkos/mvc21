<style>
    .form-signin {
        width: 100%;
        max-width: 530px;
        padding: 15px;
        margin: auto;
    }

    .form-signin .checkbox {
        font-weight: 400;
    }

    .form-signin .form-floating:focus-within {
        z-index: 2;
    }

    .form-signin input[type="email"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
    }

    .form-signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }
</style>
<main class="form-signin">
    <?php
    if (!empty($errors)) : ?>
        <div class="alert alert-danger" role="alert">
            <?php
            foreach ($errors as $error): ?>
                <span><?php
                    echo $error; ?></span><br/>
            <?php
            endforeach; ?>
        </div>
    <?php
    endif; ?>
    <form method="post" action="/user/register">
        <h1 class="h3 mb-3 fw-normal">Please sign up</h1>
        <div class="form-floating">
            <input type="text" class="form-control" id="floatingInput" name="full_name" placeholder="Full Name" value="">
            <label for="floatingInput">Full Name</label>
        </div>
        <div class="form-floating">
            <input type="email" class="form-control" id="floatingInput" name="email" placeholder="email" value="">
            <label for="floatingInput">Email</label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" id="floatingPassword" name="password"
                   placeholder="password" value="">
            <label for="floatingPassword">Password</label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" id="floatingPasswordConfirm" name="password_confirm"
                   placeholder="Password Confirm" value="">
            <label for="floatingPasswordConfirm">Password Confirm</label>
        </div>
        <button class="w-100 btn btn-lg btn-primary" type="submit">Sign up</button>
    </form>
</main>
