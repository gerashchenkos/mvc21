<main>

    <section class="py-5 text-center container">
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-light">Shopping Cart example</h1>
                <p class="lead text-muted">PHP Shopping Cart</p>
                <p>
                    <a href="/" class="btn btn-primary my-2">Products Page</a>
                    <a href="/cart" class="btn btn-secondary my-2">Cart Page</a>
                </p>
            </div>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            <form method="POST" action="/cart/add">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                <div class="col-1">
                    <?php foreach($categories as $category): ?>
                        <p><a href="/?cat_id=<?php echo $category->getId();?>"><?php echo $category->getTitle();?></a></p>
                    <?php endforeach; ?>
                </div>
                <div class="col-11">
                    <div class="row">
                <?php foreach($products as $product): ?>
                    <div class="col-4">
                        <div class="card shadow-sm">
                            <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em"><?php echo $product->getTitle();?></text></svg>

                            <div class="card-body">
                                <p class="card-text"><?php echo $product->getDescription() . " " . "$ " . $product->getPrice();?></p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <input type="checkbox" name="products[]" value="<?php echo $product->getId();?>">
                                    </div>
                                    <small class="text-muted">Quantity: <?php echo $product->getQuantity();?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                    </div>
                </div>
            </div>
                <br>
                <div class="d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary">Buy</button>
                </div>
            </form>


        </div>
    </div>

</main>