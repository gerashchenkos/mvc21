<?php

namespace MVC\Models\Interfaces;

interface CheckUnique
{
    public static function checkIfUnique(string $value, string $table, string $column): bool;
}
