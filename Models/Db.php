<?php

namespace MVC\Models;

class Db
{
    use Singleton;

    public object $pdo;

    protected function __construct()
    {
        $dsn = "mysql:host=localhost;port=3306;dbname=" . $_ENV['DB_NAME'] . ";charset=utf8";
        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_BOTH
        ];
        $this->pdo = new \PDO($dsn, $_ENV['DB_USER'], $_ENV['DB_PASS'], $options);
    }
}