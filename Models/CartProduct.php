<?php


namespace MVC\Models;


class CartProduct extends Product
{
    protected string $cart_id;
    protected int $quantity;

    public function __construct(string $id, string $cartId, string $quantity)
    {
        $product = parent::getByField("id", $id)[0];
        $this->loadFromParentObj($product);
        $this->cart_id = $cartId;
        $this->quantity = $quantity;
    }

    private function loadFromParentObj(Product $parentObj)
    {
        $objValues = get_object_vars($parentObj); // return array of object values
        foreach($objValues AS $key=>$value)
        {
            $this->$key = $value;
        }
    }

    /**
     * @return string
     */
    public function getCartId(): string
    {
        return $this->cart_id;
    }

    /**
     * @param string $cart_id
     */
    public function setCartId(string $cart_id): void
    {
        $this->cart_id = $cart_id;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    public static function getProduct($cartId, $productId): CartProduct|bool
    {
        $smtp = DB::getInstance()->pdo->prepare("
            SELECT
                *
            FROM
                `cart_products`
            WHERE
                `cart_id` = :cart_id
            AND `product_id` = :product_id
        ");
        $smtp->execute([
           "cart_id" => $cartId,
           "product_id" => $productId
        ]);
        $productData = $smtp->fetch();
        if (!empty($productData)) {
            return new CartProduct($productData['product_id'], $productData['cart_id'], $productData['quantity']);
        }
        return false;
    }

    public static function getAllProducts(string $cartId): array
    {
        $smtp = DB::getInstance()->pdo->prepare(
            "
            SELECT
                *
            FROM
                `cart_products`
            WHERE
                `cart_id` = :cart_id
        "
        );
        $smtp->execute(
            [
                "cart_id" => $cartId,
            ]
        );
        $productsData = $smtp->fetchAll();
        $products = [];
        foreach($productsData as $product){
            $products[] = self::getProduct($cartId, $product['product_id']);
        }
        return $products;
    }

    public static function add(string $cartId, array $productsIds): array
    {
        $products = [];
        foreach ($productsIds as $id) {
            $isProductAdded = self::getProduct($cartId, $id);
            if (!empty($isProductAdded)) {
                //update product quantity
            } else {
                $smtp = DB::getInstance()->pdo->prepare("
                INSERT INTO `cart_products` (
                    `cart_id`,
                    `product_id`,
                    `quantity`
                )
                VALUES
                (
                    :cart_id,
                    :product_id,
                    :quantity
                )");
                $smtp->execute([
                   "cart_id" => $cartId,
                   "product_id" => $id,
                   "quantity" => 1
                ]);
                $products[] = new CartProduct($id, $cartId, 1);
            }
        }
        return $products;
    }

}