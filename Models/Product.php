<?php

namespace MVC\Models;

class Product
{
    protected string $id;
    protected string $sku;
    protected string $category_id;
    protected string $title;
    protected string $description;
    protected float $price;
    protected int $quantity;

    public function __construct()
    {

    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getSku(): string
    {
        return $this->sku;
    }

    public function setSku(string $sku)
    {
        $this->sku = $sku;
    }

    public function getCategoryId(): string
    {
        return $this->category_id;
    }

    public function setCategoryId(string $categoryId)
    {
        $this->category_id = $categoryId;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public static function getAll(): array
    {
        $smtp = Db::getInstance()->pdo->prepare("SELECT * FROM `products`");
        $smtp->execute([]);
        $productsArray = $smtp->fetchAll();
        $products = [];
        foreach ($productsArray as $prod) {
            $product = new Product();
            $product->id = $prod['id'];
            $product->sku = $prod['sku'];
            $product->category_id = $prod['category_id'];
            $product->title = $prod['title'];
            $product->description = $prod['description'];
            $product->price = $prod['price'];
            $product->quantity = $prod['quantity'];
            $products[] = $product;
        }
        return $products;
    }

    public static function getByField(string $field, string $value): array
    {
        $smtp = Db::getInstance()->pdo->prepare("
            SELECT
                `products`.*
            FROM
                `products`
            WHERE
                `" . $field . "` = :value
        ");
        $smtp->execute(["value" => $value]);
        $productsArray =  $smtp->fetchAll();
        $products = [];
        foreach ($productsArray as $prod) {
            $product = new Product();
            $product->id = $prod['id'];
            $product->sku = $prod['sku'];
            $product->category_id = $prod['category_id'];
            $product->title = $prod['title'];
            $product->description = $prod['description'];
            $product->price = $prod['price'];
            $product->quantity = $prod['quantity'];
            $products[] = $product;
        }
        return $products;
    }
}