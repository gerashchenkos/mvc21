<?php

namespace MVC\Models;

class Cart
{
    protected string $id;
    protected string $user_id;
    protected string $ordered_at;

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->user_id;
    }

    /**
     * @param string $user_id
     */
    public function setUserId(string $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getOrderedAt(): string
    {
        return $this->ordered_at;
    }

    /**
     * @param string $ordered_at
     */
    public function setOrderedAt(string $ordered_at): void
    {
        $this->ordered_at = $ordered_at;
    }

    public function __construct(string $id, string $userId, string $orderedAt)
    {
        $this->id = $id;
        $this->user_id = $userId;
        $this->ordered_at = $orderedAt;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public static function create($userId): Cart
    {
        $smtp = Db::getInstance()->pdo->prepare("INSERT INTO `cart`(`user_id`) VALUES(:user_id)");
        $smtp->execute(["user_id" => $userId]);
        $cartId = Db::getInstance()->pdo->lastInsertId();
        return self::findById($cartId);
    }

    public static function findById(string $id): Cart
    {
        $smtp = Db::getInstance()->pdo->prepare("SELECT * FROM `cart` WHERE id = :id");
        $smtp->execute(["id" => $id]);
        $cartData = $smtp->fetch();
        return new Cart($cartData['id'], $cartData['user_id'], $cartData['ordered_at']);
    }
}