<?php
	define("ROOT_PATH", dirname(__FILE__, 2));

	require_once ROOT_PATH . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

	use MVC\Routes\WebRouter;

	session_start();

    $dotenv = Dotenv\Dotenv::createImmutable(ROOT_PATH);
    $dotenv->load();

	$router = new WebRouter([
	    "/" => ["controller" => "products", "action" => "index"],
        "user/login" => ["controller" => "user", "action" => "login"],
        "user/register" => ["controller" => "user", "action" => "register"],
        "user/logout" => ["controller" => "user", "action" => "logout"],
        "cart" => ["controller" => "cart", "action" => "index"],
        "cart/add" => ["controller" => "cart", "action" => "add"],
    ]);
    $_REQUEST['path'] = empty($_REQUEST['path']) ? "/" : $_REQUEST['path'];
    /*if (!empty($_REQUEST['path'])) {
        $controller = explode("/", $_REQUEST['path'])[0];
        $action = explode("/", $_REQUEST['path'])[1];
    } else {
        $controller = "products";
        $action = "index";
    }*/
    $router->run($_REQUEST);
?>